package ua.epam.hwFiveOffice.actuator;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;

public class HealthCheck implements HealthIndicator {
    @Override
    public Health health() {
        long result = 1;
        if (result <= 0) {
            return Health.down().withDetail("Something Result", result).build();
        }
        return Health.up().build();
    }
}
