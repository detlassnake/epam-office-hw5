package ua.epam.hwFiveOffice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ua.epam.hwFiveOffice.model.User;
import ua.epam.hwFiveOffice.repository.UserRepository;

import java.util.Optional;

@Controller
@RequestMapping(path = "/users")
public class MainController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping(path = "/all")
    public @ResponseBody
    Iterable<User> getAllUsers() {
        return userRepository.findAll();
    }

    @GetMapping(params = "id")
    public @ResponseBody
    Optional<User> getUser(Integer id) {
        return userRepository.findById(id);
    }
}