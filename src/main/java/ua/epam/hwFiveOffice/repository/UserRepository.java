package ua.epam.hwFiveOffice.repository;

import org.springframework.data.repository.CrudRepository;
import ua.epam.hwFiveOffice.model.User;

public interface UserRepository extends CrudRepository<User, Integer> { }